/**
* Задание 12 - Создать интерфейс StarWars DB для данных из SWAPI.
* 
* Используя SWAPI, вывести информацию по всех планетам с пагинацией и возможностью просмотреть доп.
* информацию в модальном окне с дозагрузкой смежных ресурсов из каждой сущности.
* 
* Данные для отображения в карточке планеты:
* 1. Наименование (name)
* 2. Диаметр (diameter)
* 3. Население (population)
* 4. Уровень гравитации (gravity)
* 5. Природные зоны (terrain)
* 6. Климатические зоны (climate)
* 
* При клике по карточке отображаем в модальном окне всю информацию 
* из карточки, а также дополнительную:
* 1. Список фильмов (films)
* - Номер эпизода (episode_id)
* - Название (title)
* - Дата выхода (release_date)
* 2. Список персонажей 
* - Имя (name)
* - Пол (gender)
* - День рождения (birth_year)
* - Наименование родного мира (homeworld -> name)
* 
* Доп. требования к интерфейсу:
* 1. Выводим 10 карточек на 1 странице
* 2. Пагинация позволяет переключаться между страницами, выводить общее количество страниц и текущие выбранные
* элементы в формате 1-10/60 для 1 страницы или 11-20/60 для второй и т.д.
* 3. Используем Bootstrap 4 для создания интерфейсов.
* 4. Добавить кнопку "Показать все" - по клику загрузит все страницы с планетами и выведет
* информацию о них в един
*/

// const getPlanet = async () => {
//     const url = 'https://swapi.dev/api/planets/';
//     const response = await fetch(url);
//     const data = await response.json();
//     const planets = data.results;
//     return planets;
// };

// const res = getPlanet();
// res.then(planets => console.log(res));


// (async () => {
//     const getAppInf = async () => {
//     const url = 'https://swapi.dev/api/planets';
//     const response = await fetch(url);
//     const data = await response.json();
//     const AppInf = data.results;
//     return AppInf;
//     };
//     const planets = await getAppInf();
//     console.log(planets)

//     const cardEl = document.querySelector('.js-cards');
//     cardEl.innerHTML = planets.map(planet => `
//     <div class="card col-md-3" style="width: 18rem;">
//             <img src="img/nasa-m35LirqP6y8-unsplash.jpg" class="card-img-top" alt="...">
//             <div class="card-body">
//               <h5 class="card-title">${planet.name}</h5>
//               <p class="card-text"></p>
//               <ul class="list-group">
//                 <li class="list-group-item d-flex justify-content-between align-items-center">
//                     Наименование (тname)
//                   <span class="badge bg-primary rounded-pill">${planet.name}</span>
//                 </li>
//                 <li class="list-group-item d-flex justify-content-between align-items-center">
//                     Диаметр (diameter)
//                   <span class="badge bg-primary rounded-pill">${planet.diameter}</span>
//                 </li>
//                 <li class="list-group-item d-flex justify-content-between align-items-center">
//                     Население (population) 
//                   <span class="badge bg-primary rounded-pill">${planet.population}</span>
//                 </li>
//                 <li class="list-group-item d-flex justify-content-between align-items-center">
//                     Уровень гравитации (gravity)
//                   <span class="badge bg-primary rounded-pill">${planet.gravity}</span>
//                 </li>
//                 <li class="list-group-item d-flex justify-content-between align-items-center">
//                     Природные зоны (terrain) 
//                   <span class="badge bg-primary rounded-pill">${planet.terrain}</span>
//                 </li>
//                 <li class="list-group-item d-flex justify-content-between align-items-center">
//                     Климатические зоны (climate)
//                   <span class="badge bg-primary rounded-pill">${planet.climate}</span>
//                 </li>
//               </ul>
//               <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
//                 Launch demo modal
//               </button>
//             </div>
//           </div>
//     `
// );

// const pageFirst = document.querySelector('.js-pag_first');
// pageFirst.classList.toggle('active');

// console.log();
// })() 


// document.querySelector('.js-pag').addEventListener(
//   'click',
//   e => {
//      console.log('fgh') 
//     if (!e.target.parentNode.classList.contains('js-pag_second')) {
//     return }
    

//     (async () => {
//       console.log('fff')
//       const getAppInf_2 = async () => {
//       const url_2 = 'http://swapi.dev/api/planets/?page=2';
//       const response_2 = await fetch(url_2);
//       const data_2 = await response_2.json();
//       const AppInf_2 = data_2.results;
//       return AppInf_2;
//       };
//       const planets_2 = await getAppInf_2();
//       console.log(planets_2)
  
//       const cardEl2 = document.querySelector('.js-card');
//       cardEl2.innerHTML = planets_2.map(planet => `
//       <div class="card col-md-3 js-card" style="width: 18rem;">
//               <img src="img/nasa-m35LirqP6y8-unsplash.jpg" class="card-img-top" alt="...">
//               <div class="card-body">
//                 <h5 class="card-title">${planet.name}</h5>
//                 <p class="card-text"></p>
//                 <ul class="list-group">
//                   <li class="list-group-item d-flex justify-content-between align-items-center">
//                       Наименование (тname)
//                     <span class="badge bg-primary rounded-pill">${planet.name}</span>
//                   </li>
//                   <li class="list-group-item d-flex justify-content-between align-items-center">
//                       Диаметр (diameter)
//                     <span class="badge bg-primary rounded-pill">${planet.diameter}</span>
//                   </li>
//                   <li class="list-group-item d-flex justify-content-between align-items-center">
//                       Население (population) 
//                     <span class="badge bg-primary rounded-pill">${planet.population}</span>
//                   </li>
//                   <li class="list-group-item d-flex justify-content-between align-items-center">
//                       Уровень гравитации (gravity)
//                     <span class="badge bg-primary rounded-pill">${planet.gravity}</span>
//                   </li>
//                   <li class="list-group-item d-flex justify-content-between align-items-center">
//                       Природные зоны (terrain) 
//                     <span class="badge bg-primary rounded-pill">${planet.terrain}</span>
//                   </li>
//                   <li class="list-group-item d-flex justify-content-between align-items-center">
//                       Климатические зоны (climate)
//                     <span class="badge bg-primary rounded-pill">${planet.climate}</span>
//                   </li>
//                 </ul>
//                 <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
//                   Launch demo modal
//                 </button>
//               </div>
//             </div>
//       `
//   );
    
  
//   console.log();
//   })() 
  
//   });


// )



// document.querySelector('.js-cards').addEventListener (
//   'click',
//   e =>
//   { console.log('gd') 
//     if (!e.target.parentNode.classList.contains('js-btn-modal')) {
//     return }
    
// (async () => {
//   const getAppInfp = async () => {
//   const urlp = 'https://swapi.dev/api/planets';
//   const responsep = await fetch(urlp);
//   const datap = await responsep.json();
//   const AppInfp = datap.results;
//   return AppInfp;
//   };
//   const planetsp = await getAppInfp();
  
//   console.log(planetsp);
//   const getFilm = async (name, films) => {
//     for (let i = 0; i < films.length; i++) {
//       const myFilmsExists = await Promise.all(
//         planets[i].films.map(
//           filmUrl => fetch(filmUrl).then(res => res.json())
//         )
//       )
//       .then(films => !!films.find(planet => planet.name === name))
//           if (myFilmsExists) {
//             return films[i];
//           }
          
//     }
//   }
// const modalcardEl = document.querySelector('.js-modal-card');
//     const homeworld = await getHomeworld ();
//     const tr = document.createElement('tr');
//     const td = document.createElement('td');
//     modalcardEl.innerHTML = 
//     // planets.find(planet => planet.name === 'name');
//     films.map(planet => `
//     <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
//   <div class="modal-dialog">
//     <div class="modal-content">
//       <div class="modal-header">
//         <h5 class="modal-title" id="exampleModalLabel">${planet.name}</h5>
//         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
//       </div>
//       <div class="modal-body">
//       <div class="card col-md-3 js-card" style="width: 18rem;">
//       <img src="img/nasa-m35LirqP6y8-unsplash.jpg" class="card-img-top" alt="...">
//       <div class="card-body">
//         <p class="card-text"></p>
//         <ul class="list-group">
//           <li class="list-group-item d-flex justify-content-between align-items-center">
//               Наименование (name)
//             <span class="badge bg-primary rounded-pill">${planet.name}</span>
//           </li>
//           <li class="list-group-item d-flex justify-content-between align-items-center">
//               Диаметр (diameter)
//             <span class="badge bg-primary rounded-pill">${planet.diameter}</span>
//           </li>
//           <li class="list-group-item d-flex justify-content-between align-items-center">
//               Население (population) 
//             <span class="badge bg-primary rounded-pill">${planet.population}</span>
//           </li>
//           <li class="list-group-item d-flex justify-content-between align-items-center">
//               Уровень гравитации (gravity)
//             <span class="badge bg-primary rounded-pill">${planet.gravity}</span>
//           </li>
//           <li class="list-group-item d-flex justify-content-between align-items-center">
//               Природные зоны (terrain) 
//             <span class="badge bg-primary rounded-pill">${planet.terrain}</span>
//           </li>
//           <li class="list-group-item d-flex justify-content-between align-items-center">
//               Климатические зоны (climate)
//             <span class="badge bg-primary rounded-pill">${planet.climate}</span>
//           </li>
//         </ul>
//       </div>
//     </div>
//     <h5 class="card-title">Список фильмов</h5>
//         <table class="table table-borderless">
//           <thead>
//             <tr>
//               <th scope="col"></th>
//               <th scope="col">Номер эпизода (episode_id)</th>
//               <th scope="col">Название (title)</th>
//               <th scope="col">Дата выхода (release_date)</th>
//             </tr>
//           </thead>
//           <tbody>
//             <tr>
//               <th scope="row">1</th>
//               <td>${film.episode_id}</td>
//               <td>${film.title}</td>
//               <td>${film.release_date}</td>
//             </tr>
            
//           </tbody>
//         </table>
//         <h5 class="card-title">Список персонажей</h5>
//         <table class="table table-borderless">
//           <thead>
//             <tr>
//               <th scope="col"></th>
//               <th scope="col">Имя (name)</th>
//               <th scope="col">Пол (gender)</th>
//               <th scope="col">День рождения (birth_year)</th>
//               <th scope="col">Наименование родного мира (homeworld.name)</th>
//             </tr>
//           </thead>
//           <tbody>
//             <tr>
//               <th scope="row">1</th>
//               <td>${film.name}</td>
//               <td>${film.gender}</td>
//               <td>${film.birth_year}</td>
//               <td>${resident.homeworld}</td>
//             </tr>
//             <tr>
//               <th scope="row">2</th>
//               <td>Jacob</td>
//               <td>Thornton</td>
//               <td>@fat</td>
//               <td>@mdo</td>
//             </tr>
//             <tr>
//               <th scope="row">3</th>
//               <td colspan="2">Larry the Bird</td>
//               <td>@twitter </td>
//               <td>@mdo</td>
//             </tr>
//           </tbody>
//         </table> 
//       </div>
//     </div>
//   </div>
// </div> 
//     `   
// );

// tr.document.querySelector('table')[0,1].appendChild('tr');
// td.document.querySelector('table')[0,1].appendChild('td');

// console.log();

// })() 
//     }
// );
  





    // 'click',

    // e => 
    // { console.log('gh')
    //     // e.target.parentNode.classList.contains('.js-btn');
       
         
    //     const div = document.createElement('div');
    //     div.classList.add('modal');
    //     div.setAttribute('tabindex', '-1')
    //     div.innerHTML = `
    // <div class="modal-dialog">
    //     <div class="modal-content">
    //      <div class="modal-header">
    //         <h5 class="modal-title">Modal title</h5>
    //         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    //      </div>
    //       <div class="modal-body">
    //          <p>Modal body text goes here.</p>
    //       </div>
    //       <div class="modal-footer">
    //           <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    //           <button type="button" class="btn btn-primary">Save changes</button>
    //       </div>
    //      </div>
    //  </div> `

    //     document.querySelector('.js-card').append(div);
    // }
    
    // );








const urls = [
  'https://swapi.dev/api/planets/?page=1' ,
  'https://swapi.dev/api/planets/?page=2' , 
  'https://swapi.dev/api/planets/?page=3' , 
  'https://swapi.dev/api/planets/?page=4' ,
  'https://swapi.dev/api/planets/?page=5' ,
  'https://swapi.dev/api/planets/?page=6' ,
  ];

 
(async ()  => {
  const getPage = async (url = urls[0]) => {
    const response = await fetch(url);
    const data = await response.json();
    const planets = data.results;
    for (let i = 0; i < planets.length; i++) {
      const  planetName = planets[i].name;
      const planetDiameter = planets[i].diameter;
      const planetPopulation = planets[i].population;
      const planetGravity = planets[i].gravity;
      const planetTerrain = planets[i].terrain;
      const planetClimate = planets[i].climate;
      const getFilms = planets[i].films
      .map(filmUrl => fetch(filmUrl).then(res => res.json())
      .then(film => `episode ${film.episode_id}. ${film.title}
      (${film.release_date})`
      ))
    const planetFilmList = await Promise
      .all(getFilms)
      .then(res => res.join(';\n'));
    const getHomeworld = async url => {
      const response = await fetch(url);
      const data = await response.json();
      const homeworld = data.name;
      return homeworld;
    }
    const  getResidents = planets[i].residents
      .map(residentUrl => fetch(residentUrl).then(res => res.json())
      .then(resident => `${resident.name}:
      gender: ${resident.gender},
      birth year: ${resident.birth_year},
      homeworld: ${getHomeworld(resident.homeworld).then(res => res)}`)
    )
    const planetResidents = await Promise
      .all(getResidents)
      .then(res => res.join(';\n'));

    const cardEl = document.querySelector('.js-page');
    cardEl.innerHTML = `
<div class="card col-md-3" style="width: 18rem;">
            <img src="img/nasa-m35LirqP6y8-unsplash.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">${planetName}</h5>
              <p class="card-text"></p>
              <ul class="list-group">
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Наименование (тname)
                  <span class="badge bg-primary rounded-pill">${planetName}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Диаметр (diameter)
                  <span class="badge bg-primary rounded-pill">${planetDiameter}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Население (population) 
                  <span class="badge bg-primary rounded-pill">${planetPopulation}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Уровень гравитации (gravity)
                  <span class="badge bg-primary rounded-pill">${planetGravity}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Природные зоны (terrain) 
                  <span class="badge bg-primary rounded-pill">${planetTerrain}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Климатические зоны (climate)
                  <span class="badge bg-primary rounded-pill">${planetClimate}</span>
                </li>
              </ul>
              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#q${planetDiameter}">
                Learn more
              </button>
            </div>

<div class="modal fade" id="q${planetDiameter}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">${planetName}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body js-modal-card">
        <div class="card col-md-3" style="width: 18rem;">
          <img src="img/nasa-m35LirqP6y8-unsplash.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">${planetName}</h5>
            <p class="card-text"></p>
            <ul class="list-group">
              <li class="list-group-item d-flex justify-content-between align-items-center">
                  Наименование (name)
                <span class="badge bg-primary rounded-pill">${planetName}</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                  Диаметр (diameter)
                <span class="badge bg-primary rounded-pill">${planetDiameter}</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                  Население (population)
                <span class="badge bg-primary rounded-pill">${planetPopulation}</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                  Уровень гравитации (gravity)
                <span class="badge bg-primary rounded-pill">${planetGravity}</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                  Природные зоны (terrain)
                <span class="badge bg-primary rounded-pill">${planetTerrain}</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                  Климатические зоны (climate) 
                <span class="badge bg-primary rounded-pill">${planetClimate}</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                  Списмок фильмов 
                <span class="badge bg-primary rounded-pill">${planetFilmList}</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                  Список персонажей
                <span class="badge bg-primary rounded-pill">${planetResidents}</span>
              </li>
            </ul> 
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div> 

</div>`;

   
    }
    const pageNum = urls.indexOf(url) + 1;
    const pageId = () => {
      switch (pageNum) {
        case 1:
          return '1-10/60';
        case 2:
          return '11-20/60';
        case 3:
          return '21-30/60';
        case 4:
          return '31-40/60';  
        case 5:
          return '41-50/60';
        case 2:
          return '51-60/60';
        default:
          return 'I do not know where I am'
      }
    }
const navContEl = document.querySelector('.navigation-container');
const newNav = document.createElement('nav');
newNav.setAttribute('aria-label', 'Page navigation example');
newNav.innerHTML =`
<ul class="pagination justify-content-center">
   <li class="page-item" mode="${pageNum - 1}">
      <a class="page-link" aria-label="Previous">
         <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li class="page-item"><a class="page-link" href="#">${pageId()}</a></li>
    <li class="page-item" mode="${pageNum + 1}">
      <a class="page-link" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
</ul>`;
navContEl.appendChild(newNav);
if (pageNum === 1) {
  document.querySelector('.pagination li').classList.add('disabled')
}
if (pageNum === 6) {
  document.querySelector('.pagination li')[2].classList.add('disabled')
}
  }
return await getPage();
})();



document.querySelector('.js-btn-view_all').addEventListener (
  'click',
  e => {
    (async ()  => {
      for (let j=0; j<url.length; j++) {
        const getPage = async (url = urls[j]) => {
        const response = await fetch(url);
        const data = await response.json();
        const planets = data.results;
        for (let i = 0; i < planets.length; i++) {
          const  planetName = planets[i].name;
          const planetDiameter = planets[i].diameter;
          const planetPopulation = planets[i].population;
          const planetGravity = planets[i].gravity;
          const planetTerrain = planets[i].terrain;
          const planetClimate = planets[i].climate;
          const getFilms = planets[i].films
          .map(filmUrl => fetch(filmUrl).then(res => res.json())
          .then(film => `episode ${film.episode_id}. ${film.title}
          (${film.release_date})`
          ))
        const planetFilmList = await Promise
          .all(getFilms)
          .then(res => res.join(';\n'));
        const getHomeworld = async url => {
          const response = await fetch(url);
          const data = await response.json();
          const homeworld = data.name;
          return homeworld;
        }
        const  getResidents = planets[i].residents
          .map(residentUrl => fetch(residentUrl).then(res => res.json())
          .then(resident => `${resident.name}:
          gender: ${resident.gender},
          birth year: ${resident.birth_year},
          homeworld: ${getHomeworld(resident.homeworld).then(res => res)}`)
        )
        const planetResidents = await Promise
          .all(getResidents)
          .then(res => res.join(';\n'));
        }
      res.push(j)
      }
     }
      
      const cardElall = document.createElement('div');
      cardElall.classList.add('container');
      cardElall.innerHTML = `
       <div class="card col-md-6" style="width: 18rem;">
       <img src="..." class="card-img-top" alt="...">
       <div class="card-body">
       <h5 class="card-title">${planetName}</h5>
        
       
       
      </div>
      </div>
       `

       NewPlanetCards.forEach(cardElall => document.querySelector('.js-page').appendChild(cardElall));
        
       } )();
      
    }
);
 
  